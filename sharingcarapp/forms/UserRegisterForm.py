# -*- coding: utf-8 -*-
from datetime import date
from django import forms
from django.contrib.auth.models import User
from django.core.validators import RegexValidator

__author__ = 'Ruben'
phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',message="El número de teléfono de estar en este formato: '+999999999'. Hasta 15 carácteres permitidos.")
class UserRegisterForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Nombre de usuario'}),max_length=32, label="Nombre de usuario")
    password = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Contraseña', 'type':'password'}),max_length=32, label="Contraseña")
    confirm_password = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Confirmar contraseña', 'type':'password'}),max_length=32, label="Confirmar contraseña")
    name = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Nombre'}),max_length=64, label="Nombre")
    surnames = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Apellidos'}),max_length=128, label="Apellidos")
    city = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Ciudad'}),max_length=128, label="Ciudad")
    birthdate = forms.DateField(label='Fecha de nacimiento', widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : date.today()}))
    phone = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : '+034 999999999'}),validators=[phone_regex], label='Número de teléfono', max_length=64)
    photo = forms.ImageField(widget=forms.FileInput(attrs={'type' : 'file'}),label= 'Imagen', required=False)
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class' : 'form-control', 'placeholder' : 'Email'}),label= 'Email', required=True)

    def clean(self):
        password = self.cleaned_data['password']
        confirm_password = self.cleaned_data['confirm_password']
        if password != confirm_password:
            self.add_error('password', "Las contraseñas no coinciden.")
            self.add_error('confirm_password', "Las contraseñas no coinciden.")

        if User.objects.filter(username = self.cleaned_data['username']).exists():
            self.add_error('username', "Ya existe ese nombre de usuario")

        if User.objects.filter(email = self.cleaned_data['email']).exists():
            self.add_error('email', "Ya existe un usuario con ese email")
