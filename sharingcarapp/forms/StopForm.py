from django import forms

__author__ = 'Ruben'

class StopForm(forms.Form):
    stop = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Parada'}),max_length=64, label='Parada')