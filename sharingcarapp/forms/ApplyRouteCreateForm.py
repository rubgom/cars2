# -*- coding: utf-8 -*-
from django import forms
from django.core.validators import MinValueValidator

__author__ = 'Ruben'

class ApplyRouteCreateForm(forms.Form):
    comment = forms.CharField(widget=forms.Textarea(attrs={'class' : 'form-control', 'placeholder' : 'Escriba lo que quiera comunicarle al dueño del anuncio.'}), label="Comentario", required=True)

