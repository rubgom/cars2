# -*- coding: utf-8 -*-
from django import forms
from django.core.validators import MinValueValidator
import datetime

__author__ = 'Ruben'
ROUTE_KIND = (('','Elija una opción'),('1','Oferta'),('2','Demanda'))

class CreateAnnouncementForm(forms.Form):
    origin = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Origen'}),max_length=64, label="Origen", required=True)
    destination = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Destino'}),max_length=64, label="Destino", required=True)
    kind = forms.ChoiceField(widget=forms.Select(attrs={'class' : 'form-control'}), label='Tipo de anuncio', required=True, choices=ROUTE_KIND)
    seating = forms.IntegerField(widget=forms.NumberInput(attrs={'class' : 'form-control', 'placeholder' : 'Numero de plazas', 'min': '0'}),validators=[MinValueValidator(0)], label="Plazas")
    unitPrice = forms.DecimalField(widget=forms.NumberInput(attrs={'class' : 'form-control', 'placeholder' : 'Precio por persona', 'min': '0'}),max_digits=5, decimal_places=2, validators=[MinValueValidator(0)], label='Precio')
    description = forms.CharField(widget=forms.Textarea(attrs={'class' : 'form-control', 'placeholder' : 'Descripción'}),label="Descripción", required=True)
    date = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Hora de salida'}), initial=datetime.date.today(), label="Fecha")
    departTime = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Hora de salida'}),max_length=32, label="Hora de salida", required=False)
