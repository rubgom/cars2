# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.core.validators
import sharingcarapp.helpers.User


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Actor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Administrator',
            fields=[
                ('actor_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='sharingcarapp.Actor')),
            ],
            options={
            },
            bases=('sharingcarapp.actor',),
        ),
        migrations.CreateModel(
            name='Announcement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('origin', models.CharField(max_length=256)),
                ('destination', models.CharField(max_length=256)),
                ('description', models.TextField()),
                ('seating', models.IntegerField(validators=[django.core.validators.MinValueValidator(0)])),
                ('unitPrice', models.DecimalField(max_digits=5, decimal_places=2, validators=[django.core.validators.MinValueValidator(0)])),
                ('date', models.DateTimeField()),
                ('creationMoment', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ApplyAnnouncement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creationDate', models.DateTimeField(auto_now_add=True)),
                ('comment', models.TextField()),
                ('state', models.CharField(default=b'1', max_length=64)),
                ('announcement', models.ForeignKey(to='sharingcarapp.Announcement')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ApplyRoute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creationDate', models.DateTimeField(auto_now_add=True)),
                ('comment', models.TextField()),
                ('state', models.CharField(default=b'1', max_length=64)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subject', models.CharField(max_length=256)),
                ('comment', models.TextField()),
                ('rating', models.IntegerField(validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(10)])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CommentAnnouncement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subject', models.CharField(max_length=128)),
                ('comment', models.TextField()),
                ('rating', models.IntegerField(validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(10)])),
                ('announcement', models.ForeignKey(to='sharingcarapp.Announcement')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CommentRoute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subject', models.CharField(max_length=128)),
                ('comment', models.TextField()),
                ('rating', models.IntegerField(validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(10)])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Day',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('day', models.IntegerField()),
                ('departTime', models.CharField(max_length=64)),
                ('returnTime', models.CharField(max_length=64)),
                ('active', models.BooleanField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Folder',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=256)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subject', models.CharField(max_length=256)),
                ('body', models.TextField(max_length=256)),
                ('creationMoment', models.DateTimeField(auto_now_add=True)),
                ('folder', models.ForeignKey(to='sharingcarapp.Folder')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Route',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('origin', models.CharField(max_length=256)),
                ('destination', models.CharField(max_length=256)),
                ('visibility', models.BooleanField(default=True)),
                ('description', models.TextField()),
                ('kind', models.CharField(max_length=64)),
                ('seating', models.IntegerField(validators=[django.core.validators.MinValueValidator(0)])),
                ('unitPrice', models.DecimalField(max_digits=5, decimal_places=2, validators=[django.core.validators.MinValueValidator(0)])),
                ('creationMoment', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='StopAnnouncement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('stop', models.CharField(max_length=256)),
                ('sequence', models.IntegerField(validators=[django.core.validators.MinValueValidator(0)])),
                ('announcement', models.ForeignKey(to='sharingcarapp.Announcement')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='StopRoute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('stop', models.CharField(max_length=256)),
                ('sequence', models.IntegerField(validators=[django.core.validators.MinValueValidator(0)])),
                ('route', models.ForeignKey(to='sharingcarapp.Route')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('actor_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='sharingcarapp.Actor')),
                ('name', models.CharField(max_length=256)),
                ('surnames', models.CharField(max_length=256)),
                ('city', models.CharField(max_length=256)),
                ('birthdate', models.DateField()),
                ('phone', models.CharField(max_length=256, validators=[django.core.validators.RegexValidator(b'^\\+?\\d+$')])),
                ('searchinCar', models.BooleanField(default=False)),
                ('photo', models.ImageField(default=b'default', null=True, upload_to=sharingcarapp.helpers.User.path_generator)),
            ],
            options={
            },
            bases=('sharingcarapp.actor',),
        ),
        migrations.AddField(
            model_name='route',
            name='user',
            field=models.ForeignKey(to='sharingcarapp.User'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='message',
            name='recipient',
            field=models.ForeignKey(related_name='recipient', to='sharingcarapp.Actor'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='message',
            name='sender',
            field=models.ForeignKey(related_name='sender', to='sharingcarapp.Actor'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='folder',
            name='actor',
            field=models.ForeignKey(to='sharingcarapp.Actor'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='day',
            name='route',
            field=models.ForeignKey(to='sharingcarapp.Route'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='commentroute',
            name='route',
            field=models.ForeignKey(to='sharingcarapp.Route'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='commentroute',
            name='user',
            field=models.ForeignKey(to='sharingcarapp.User'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='commentannouncement',
            name='user',
            field=models.ForeignKey(to='sharingcarapp.User'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='comment',
            name='evaluated',
            field=models.ForeignKey(related_name='evaluated', to='sharingcarapp.User'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='comment',
            name='referrer',
            field=models.ForeignKey(related_name='referrer', to='sharingcarapp.User'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='applyroute',
            name='route',
            field=models.ForeignKey(to='sharingcarapp.Route'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='applyroute',
            name='user',
            field=models.ForeignKey(to='sharingcarapp.User'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='applyannouncement',
            name='user',
            field=models.ForeignKey(to='sharingcarapp.User'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='announcement',
            name='user',
            field=models.ForeignKey(to='sharingcarapp.User'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='actor',
            name='user_account',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, unique=True),
            preserve_default=True,
        ),
    ]
