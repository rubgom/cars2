# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('sharingcarapp', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='announcement',
            name='kind',
            field=models.CharField(default=datetime.datetime(2016, 2, 21, 15, 51, 33, 730414, tzinfo=utc), max_length=64),
            preserve_default=False,
        ),
    ]
