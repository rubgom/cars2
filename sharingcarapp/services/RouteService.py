from django.db import transaction
from sharingcarapp.models.Day import Day
from sharingcarapp.models.Route import Route
from sharingcarapp.models.User import User

__author__ = 'Ruben'

class RouteService:
    @staticmethod
    @transaction.atomic
    def save(origin, destination, kind, seating, unitPrice, monday_departTime, monday_returnTime, tuesday_departTime,
             tuesday_returnTime, wednesday_departTime, wednesday_returnTime, thursday_departTime, thursday_returnTime,
             friday_departTime, friday_returnTime, saturday_departTime, saturday_returnTime, sunday_departTime,
             sunday_returnTime, description, userId):
        route = Route()
        user = User.objects.filter(user_account__id = userId)[0]
        route.user = user
        route.origin = origin
        route.destination = destination
        route.description = description
        route.kind = kind
        route.seating = seating
        route.unitPrice = unitPrice
        route.save()

        # Monday
        monday = Day()
        monday.day = "1"
        if ((monday_departTime != "") and (monday_returnTime != "")):
            monday.active = True
            monday.departTime = monday_departTime
            monday.returnTime = monday_returnTime
        else:
            monday.active = False
        monday.route = route
        monday.save()

        # Tuesday
        tuesday = Day()
        tuesday.day = "2"
        if ((tuesday_departTime != "") and (tuesday_returnTime != "")):
            tuesday.active = True
            tuesday.departTime = tuesday_departTime
            tuesday.returnTime = tuesday_returnTime
        else:
            tuesday.active = False
        tuesday.route = route
        tuesday.save()

        # Wednesday
        wednesday = Day()
        wednesday.day = "3"
        if ((wednesday_departTime != "") and (wednesday_returnTime != "")):
            wednesday.active = True
            wednesday.departTime = wednesday_departTime
            wednesday.returnTime = wednesday_returnTime
        else:
            wednesday.active = False
        wednesday.route = route
        wednesday.save()

        # Thusday
        thursday = Day()
        thursday.day = "4"
        if ((thursday_departTime != "") and (thursday_returnTime != "")):
            thursday.active = True
            thursday.departTime = thursday_departTime
            thursday.returnTime = thursday_returnTime
        else:
            thursday.active = False
        thursday.route = route
        thursday.save()

        # Friday
        friday = Day()
        friday.day = "5"
        if ((friday_departTime != "") and (friday_returnTime != "")):
            friday.active = True
            friday.departTime = friday_departTime
            friday.returnTime = friday_returnTime
        else:
            friday.active = False
        friday.route = route
        friday.save()

        # Saturday
        saturday = Day()
        saturday.day = "6"
        if ((saturday_departTime != "") and (saturday_returnTime != "")):
            saturday.active = True
            saturday.departTime = saturday_departTime
            saturday.returnTime = saturday_returnTime
        else:
            saturday.active = False
        saturday.route = route
        saturday.save()

        # Sunday
        sunday = Day()
        sunday.day = "7"
        if ((sunday_departTime != "") and (sunday_returnTime != "")):
            sunday.active = True
            sunday.departTime = sunday_departTime
            sunday.returnTime = sunday_returnTime
        else:
            sunday.active = False
        sunday.route = route
        sunday.save()

        return route