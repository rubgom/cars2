from django.db import transaction
from sharingcarapp.models.Day import Day
from sharingcarapp.models.Route import Route
from sharingcarapp.models.StopRoute import StopRoute
from sharingcarapp.models.StopAnnouncement import StopAnnouncement
from sharingcarapp.models.User import User

__author__ = 'Ruben'

class StopService:
    @staticmethod
    @transaction.atomic
    def saveStopRoute(stop, route):
        stopRoute = StopRoute()
        stopRoute.stop = stop
        stopRoute.route = route
        stopRoute.sequence = StopRoute.objects.filter(route = route).count() + 1
        stopRoute.save()
        return stopRoute

    @staticmethod
    @transaction.atomic
    def saveStopAnnouncement(stop, announcement):
        stopAnnouncement = StopAnnouncement()
        stopAnnouncement.stop = stop
        stopAnnouncement.announcement = announcement
        stopAnnouncement.sequence = StopAnnouncement.objects.filter(announcement = announcement).count() + 1
        stopAnnouncement.save()
        return stopAnnouncement
