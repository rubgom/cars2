from django.db import transaction
from sharingcarapp.models.Announcement import Announcement
from sharingcarapp.models.User import User


class AnnouncementService:
    @staticmethod
    @transaction.atomic
    def save(origin, destination, kind, seating, unitPrice, date, departTime, description, userId):
        announcement = Announcement()
        user = User.objects.filter(user_account__id = userId)[0]
        announcement.user = user
        announcement.origin = origin
        announcement.destination = destination
        announcement.description = description
        announcement.kind = kind
        announcement.seating = seating
        announcement.unitPrice = unitPrice
        announcement.date = date
        announcement.departTime = departTime
        announcement.save()
        return announcement
