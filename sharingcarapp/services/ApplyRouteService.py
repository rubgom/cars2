from django.db import transaction
from sharingcarapp.models.ApplyRoute import ApplyRoute
from sharingcarapp.models.CommentRoute import CommentRoute
from sharingcarapp.models.Route import Route
from sharingcarapp.models.User import User

__author__ = 'Ruben'

class ApplyRouteService:

    @staticmethod
    @transaction.atomic
    def save(comment, routeId, userId):
        applyRoute = ApplyRoute()
        applyRoute.comment = comment
        user = User.objects.filter(user_account__id = userId)[0]
        route = Route.objects.filter(id = routeId)[0]
        applyRoute.user = user
        applyRoute.route = route
        applyRoute.save()
        return  applyRoute