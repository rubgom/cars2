from django.contrib import auth
from django.db import transaction
from django.db.models import Q
from sharingcarapp.models.Route import Route
from sharingcarapp.models.User import User
__author__ = 'Ruben'

class UserService():
    @staticmethod
    @transaction.atomic
    def save(username, password, name, surnames, city, bithdate, phone, photo, email):

        user_account = auth.models.User.objects.create_user(username=username, email=email, password=password)
        user = User()
        user.name = name
        user.surnames = surnames
        user.user_account = user_account
        user.birthdate = bithdate
        user.city = city
        user.phone = phone
        user.photo = photo
        user.save()

    @staticmethod
    def calculate_rating(comments):
        rating = 0
        quantity = len(comments)
        if quantity != 0:
            sum = 0
            for comment in comments:
                sum = sum + comment.rating
            rating = int(sum/quantity)
        return rating, quantity

    @staticmethod
    def recommendations(userId):
        userRoutes = Route.objects.filter(user__user_account__id = userId)
        routes = Route.objects.filter(~Q(user__user_account__id = userId), visibility = 1)
        res = set()
        for userRoute in userRoutes:
            for route in routes:
                if userRoute.origin == route.origin and userRoute.destination == route.destination:
                    res.add(route)
        return res


