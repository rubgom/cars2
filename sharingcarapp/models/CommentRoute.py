from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from sharingcarapp.models.Route import Route
from sharingcarapp.models.User import User

__author__ = 'Ruben'


class CommentRoute(models.Model):
    subject = models.CharField(max_length=128, blank=False)
    comment = models.TextField(blank=False)
    rating = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(10)])

    user = models.ForeignKey(User)
    route = models.ForeignKey(Route)