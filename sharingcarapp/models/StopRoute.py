from django.core.validators import MinValueValidator
from django.db import models
from sharingcarapp.models.Route import Route

__author__ = 'Ruben'


class StopRoute(models.Model):
    stop = models.CharField(max_length=256, blank=False)
    sequence = models.IntegerField(validators=[MinValueValidator(0)])

    route = models.ForeignKey(Route)