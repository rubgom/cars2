from django.db import models
from sharingcarapp.models.Route import Route
from sharingcarapp.models.User import User

__author__ = 'Ruben'

class ApplyRoute(models.Model):
    creationDate = models.DateTimeField(auto_now_add=True)
    comment = models.TextField(blank=False)
    state = models.CharField(max_length=64, default="1")

    route = models.ForeignKey(Route)
    user = models.ForeignKey(User)