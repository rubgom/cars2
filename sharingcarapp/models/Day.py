from django.db import models
from sharingcarapp.models.Route import Route

__author__ = 'Ruben'

class Day(models.Model):
    day = models.IntegerField()
    departTime = models.CharField(max_length=64)
    returnTime = models.CharField(max_length=64)
    active = models.BooleanField()

    route = models.ForeignKey(Route)