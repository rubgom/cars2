from django.db import models
from Actor import Actor
from django.core.validators import *
import sharingcarapp.helpers.User


class User(Actor):
    name = models.CharField(max_length=256, blank=False)
    surnames = models.CharField(max_length=256, blank=False)
    city = models.CharField(max_length=256, blank=False)
    birthdate = models.DateField(blank=False)
    phone = models.CharField(max_length=256, blank=False, validators=[RegexValidator('^\+?\d+$')])
    searchinCar = models.BooleanField(default=False)
    photo = models.ImageField(upload_to=sharingcarapp.helpers.User.path_generator, null=True,default='default')


