from django.db import models
from sharingcarapp.models.Actor import Actor


class Folder(models.Model):
    name = models.CharField(max_length=256, blank=False)

    actor = models.ForeignKey(Actor)

