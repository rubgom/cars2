from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from sharingcarapp.models.User import User

__author__ = 'Ruben'


class Comment(models.Model):
    subject = models.CharField(max_length=256, blank=False)
    comment = models.TextField(blank=False)
    rating = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(10)])

    referrer = models.ForeignKey(User, related_name='referrer')
    evaluated = models.ForeignKey(User, related_name='evaluated')