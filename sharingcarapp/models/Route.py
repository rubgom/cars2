from django.core.validators import MinValueValidator
from django.db import models
from sharingcarapp.models.User import User


class Route(models.Model):
    origin = models.CharField(max_length=256, blank=False)
    destination = models.CharField(max_length=256, blank=False)
    visibility = models.BooleanField(default=True)
    description = models.TextField(blank=False)
    kind = models.CharField(max_length=64, blank=False)
    seating = models.IntegerField(validators=[MinValueValidator(0)], blank=False)
    unitPrice = models.DecimalField(max_digits=5, decimal_places=2, validators=[MinValueValidator(0)], blank=False)
    creationMoment = models.DateTimeField(auto_now=False, auto_now_add=True)

    user = models.ForeignKey(User)

