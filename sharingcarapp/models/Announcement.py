from django.core.validators import MinValueValidator
from django.db import models
from sharingcarapp.models.User import User

__author__ = 'Ruben'


class Announcement(models.Model):
    origin = models.CharField(max_length=256, blank=False)
    destination = models.CharField(max_length=256, blank=False)
    description = models.TextField(blank=False)
    kind = models.CharField(max_length=64, blank=False)
    seating = models.IntegerField(validators=[MinValueValidator(0)],
                                  blank=False)
    unitPrice = models.DecimalField(max_digits=5, decimal_places=2,
                                    validators=[MinValueValidator(0)],
                                    blank=False)
    date = models.DateField(blank=False)
    creationMoment = models.DateTimeField(auto_now=False, auto_now_add=True)
    departTime = models.CharField(max_length=64)

    user = models.ForeignKey(User)
