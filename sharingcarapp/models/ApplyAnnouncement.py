from django.db import models
from sharingcarapp.models.Announcement import Announcement
from sharingcarapp.models.User import User

__author__ = 'Ruben'

class ApplyAnnouncement(models.Model):
    creationDate = models.DateTimeField(auto_now_add=True)
    comment = models.TextField(blank=False)
    state = models.CharField(max_length=64, default="1")

    announcement = models.ForeignKey(Announcement)
    user = models.ForeignKey(User)