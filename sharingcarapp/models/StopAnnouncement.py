from django.core.validators import MinValueValidator
from django.db import models
from sharingcarapp.models.Announcement import Announcement

__author__ = 'Ruben'


class StopAnnouncement(models.Model):
    stop = models.CharField(max_length=256, blank=False)
    sequence = models.IntegerField(validators=[MinValueValidator(0)])

    announcement = models.ForeignKey(Announcement)