from django.contrib.auth.models import User
from django.db import models


class Actor(models.Model):
    user_account = models.ForeignKey(User, unique=True)

