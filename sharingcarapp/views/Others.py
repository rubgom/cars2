from django.views.generic import View
from django.shortcuts import render


class Others(View):

    @staticmethod
    def cookies(request):
        return render(request, "cookies.html")


