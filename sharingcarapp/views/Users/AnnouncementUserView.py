from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.views.generic import View
from sharingcarapp.forms.CreateAnnouncementForm import CreateAnnouncementForm
from sharingcarapp.services.AnnouncementService import AnnouncementService


class AnnouncementUserView(View):
    @staticmethod
    @login_required()
    def create(request):
        form = CreateAnnouncementForm()
        return render(request, 'announcement/create.html', {'form': form})

    @staticmethod
    @login_required()
    def save(request):
        userId = request.user.id
        if request.method == "POST":
            form = CreateAnnouncementForm(request.POST)
            try:
                if form.is_valid():
                        announcement = AnnouncementService.save(form.cleaned_data["origin"], form.cleaned_data["destination"],
                                                      form.cleaned_data["kind"], form.cleaned_data["seating"],
                                                      form.cleaned_data["unitPrice"],form.cleaned_data["date"],
                                                      form.cleaned_data["departTime"],form.cleaned_data["description"],userId)
                        if announcement.kind == "1":
                            result = redirect('/announcement/user/addStop?announcementId=%s' % unicode(announcement.id))
                        else:
                            result = redirect('/')
                else:
                    # form invalid
                        variables = {'form': form}
                        result = render(request, 'announcement/create.html', variables)
            except Exception as e:
                if e.message == '':
                    variables = {'form': form, 'error': e.args[1]}
                else:
                    variables = {'form': form, 'error': e.message}

                result = render(request, 'announcement/create.html', variables)
        else:
            result = redirect('create_announcement')

        return result
