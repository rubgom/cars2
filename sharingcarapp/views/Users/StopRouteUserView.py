from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.views.generic import View
from sharingcarapp.forms.StopForm import StopForm
from sharingcarapp.models.Route import Route
from sharingcarapp.services.StopService import StopService

__author__ = 'Ruben'

class StopRouteUserView(View):
    @staticmethod
    @login_required()
    def create(request):
        routeId = request.GET.get('routeId')
        form = StopForm()
        return render(request, 'stopRoute/create.html', {'form' : form, 'routeId' : routeId})

    @staticmethod
    @login_required()
    def saveStop(request):
        userId  = request.user.id
        if request.method == "POST":
            routeId = request.POST.get('routeId')
            route = Route.objects.filter(id = routeId)[0]
            form = StopForm(request.POST)
            try:
                if form.is_valid():
                    if route.user.user_account.id != userId:
                        raise Exception('No puedes modificar una ruta que no te pertenece')
                    stop = StopService.saveStopRoute(form.cleaned_data["stop"], route)
                    result = redirect('/route/show?routeId='+str(routeId))
                else:
                    # form invalid
                    variables = {'form': form, 'routeId' : routeId}
                    result = render(request, 'stopRoute/create.html', variables)
            except Exception as e:
                if e.message == '':
                    variables = {'form': form, 'exceptionError': e.args[1]}
                else:
                    variables = {'form': form, 'exceptionError': e.message}
                result = render(request, 'stopRoute/create.html', variables)
        else:
            result = redirect('create_stop_route')

        return result

    @staticmethod
    @login_required()
    def saveMoreStop(request):
        userId  = request.user.id
        if request.method == "POST":
            routeId = request.POST.get('routeId')
            route = Route.objects.filter(id = routeId)[0]
            form = StopForm(request.POST)
            try:
                if form.is_valid():
                    if route.user.user_account.id != userId:
                        raise Exception('No puedes modificar una ruta que no te pertenece')
                    stop = StopService.saveStopRoute(form.cleaned_data["stop"], route)
                    result = redirect('/route/user/addStop?routeId='+str(routeId))
                else:
                    # form invalid
                    variables = {'form': form, 'routeId' : routeId}
                    result = render(request, 'stopRoute/create.html', variables)
            except Exception as e:
                if e.message == '':
                    variables = {'form': form, 'exceptionError': e.args[1]}
                else:
                    variables = {'form': form, 'exceptionError': e.message}
                result = render(request, 'stopRoute/create.html', variables)
        else:
            result = redirect('create_stop_route')

        return result