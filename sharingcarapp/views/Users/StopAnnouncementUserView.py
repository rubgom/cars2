from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.views.generic import View
from sharingcarapp.forms.StopForm import StopForm
from sharingcarapp.models.Announcement import Announcement
from sharingcarapp.services.StopService import StopService

__author__ = 'Ruben'


class StopAnnouncementUserView(View):
    @staticmethod
    @login_required()
    def create(request):
        announcementId = request.GET.get('announcementId')
        form = StopForm()
        return render(request, 'stopAnnouncement/create.html',
                      {'form': form, 'announcementId': announcementId})

    @staticmethod
    @login_required()
    def saveStop(request):
        userId = request.user.id
        if request.method == "POST":
            announcementId = request.POST.get('announcementId')
            announcement = Announcement.objects.filter(id=announcementId)[0]
            form = StopForm(request.POST)
            try:
                if form.is_valid():
                    if announcement.user.user_account.id != userId:
                        raise Exception('No puedes modificar una ruta que no te pertenece')
                    StopService.saveStopAnnouncement(form.cleaned_data["stop"], announcement)
                    result = redirect('/announcement/show?announcementId='+str(announcementId))
                else:
                    # form invalid
                    variables = {'form': form, 'announcementId' : announcementId}
                    result = render(request, 'stopAnnouncement/create.html', variables)
            except Exception as e:
                if e.message == '':
                    variables = {'form': form, 'exceptionError': e.args[1]}
                else:
                    variables = {'form': form, 'exceptionError': e.message}
                result = render(request, 'stopAnnouncement/create.html', variables)
        else:
            result = redirect('create_stop_announcement')

        return result

    @staticmethod
    @login_required()
    def saveMoreStop(request):
        userId  = request.user.id
        if request.method == "POST":
            announcementId = request.POST.get('announcementId')
            announcement = Announcement.objects.filter(id = announcementId)[0]
            form = StopForm(request.POST)
            try:
                if form.is_valid():
                    if announcement.user.user_account.id != userId:
                        raise Exception('No puedes modificar una ruta que no te pertenece')
                    stop = StopService.saveStopAnnouncement(form.cleaned_data["stop"], announcement)
                    result = redirect('/announcement/user/addStop?announcementId='+str(announcementId))
                else:
                    # form invalid
                    variables = {'form': form, 'announcementId' : announcementId}
                    result = render(request, 'stopAnnouncement/create.html', variables)
            except Exception as e:
                if e.message == '':
                    variables = {'form': form, 'exceptionError': e.args[1]}
                else:
                    variables = {'form': form, 'exceptionError': e.message}
                result = render(request, 'stopAnnouncement/create.html', variables)
        else:
            result = redirect('create_stop_announcement')

        return result
