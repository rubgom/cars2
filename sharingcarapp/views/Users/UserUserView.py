from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.views.generic import View
from sharingcarapp.forms.UserRegisterForm import UserRegisterForm
from sharingcarapp.services.UserService import UserService

__author__ = 'Ruben'

class UserUserView(View):
    @staticmethod
    def create(request):
        form = UserRegisterForm()
        return render(request, 'user/create.html', {'form':form})

    @staticmethod
    def save(request):
        if request.method == "POST":
            form = UserRegisterForm(request.POST, request.FILES)
            try:
                if form.is_valid():
                        user = UserService.save(form.cleaned_data["username"], form.cleaned_data["password"],
                                                form.cleaned_data["name"], form.cleaned_data["surnames"], form.cleaned_data["city"],
                                                form.cleaned_data["birthdate"],form.cleaned_data["phone"],
                                                form.cleaned_data["photo"],form.cleaned_data["email"])

                        result = redirect('/')
                else:
                    # form invalid
                        variables = {'form': form}
                        result = render(request, 'user/create.html', variables)
            except Exception as e:
                if e.message == '':
                    variables = {'form': form, 'errorException': e.args[1]}
                else:
                    variables = {'form': form, 'errorException': e.message}

                result = render(request, 'user/create.html', variables)
        else:
            result = redirect('user_register')

        return result

    @staticmethod
    @login_required()
    def recommendations(request):
        routes = UserService.recommendations(request.user.id)
        return render(request, 'route/list2.html', {'routes' : routes})