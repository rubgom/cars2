from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.views.generic import View
from sharingcarapp.forms.CreateRouteForm import CreateRouteForm
from sharingcarapp.services.RouteService import RouteService

__author__ = 'Ruben'

class RouteUserView(View):
    @staticmethod
    @login_required()
    def create(request):
        form = CreateRouteForm()
        return render(request, 'route/create.html', {'form' : form})

    @staticmethod
    @login_required()
    def save(request):
        userId  = request.user.id
        if request.method == "POST":
            form = CreateRouteForm(request.POST)
            try:
                if form.is_valid():
                        route = RouteService.save(form.cleaned_data["origin"], form.cleaned_data["destination"],
                                                      form.cleaned_data["kind"], form.cleaned_data["seating"],
                                                      form.cleaned_data["unitPrice"],form.cleaned_data["monday_departTime"],
                                                      form.cleaned_data["monday_returnTime"],form.cleaned_data["tuesday_departTime"],form.cleaned_data["tuesday_returnTime"],
                                                      form.cleaned_data["wednesday_departTime"],form.cleaned_data["wednesday_returnTime"],form.cleaned_data["thursday_departTime"],
                                                      form.cleaned_data["thursday_returnTime"],form.cleaned_data["friday_departTime"],form.cleaned_data["friday_returnTime"],
                                                      form.cleaned_data["saturday_departTime"],form.cleaned_data["saturday_returnTime"],form.cleaned_data["sunday_departTime"],
                                                      form.cleaned_data["sunday_returnTime"],form.cleaned_data["description"],userId)
                        if route.kind == "1":
                            result = redirect('/route/user/addStop?routeId='+ unicode(route.id))
                        else:
                            result = redirect('/')
                else:
                    # form invalid
                        variables = {'form': form}
                        result = render(request, 'route/create.html', variables)
            except Exception as e:
                if e.message == '':
                    variables = {'form': form, 'error': e.args[1]}
                else:
                    variables = {'form': form, 'error': e.message}

                result = render(request, 'route/create.html', variables)
        else:
            result = redirect('create_route')

        return result