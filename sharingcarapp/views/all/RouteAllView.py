from django.db.models import Sum
from django.shortcuts import render
from django.views.generic import View
from sharingcarapp.models.Comment import Comment
from sharingcarapp.models.CommentRoute import CommentRoute
from sharingcarapp.models.Route import Route
from sharingcarapp.services.UserService import UserService

__author__ = 'Ruben'

class RouteAllView(View):
    @staticmethod
    def all(request):
        routes = Route.objects.filter(visibility = 1, seating__gt = 0).order_by('-creationMoment')
        return render(request, 'route/list2.html', {'routes' : routes})

    @staticmethod
    def show(request):
        routeId = request.GET.get('routeId')
        route = Route.objects.filter(visibility = 1, id = routeId)[0]
        comments = Comment.objects.filter(evaluated = route.user.id)
        rating, quantity = UserService.calculate_rating(comments)
        commentsRouteLen = len(route.commentroute_set.all())
        if commentsRouteLen > 0:
            ratingRoute = int(CommentRoute.objects.filter(route__id = routeId).aggregate(Sum('rating')).get('rating__sum')/commentsRouteLen)
        else:
            ratingRoute = 0
        return render(request, 'route/show.html', {'route' : route, 'rating' : rating, 'quantity' : quantity, 'ratingRoute' : ratingRoute})
