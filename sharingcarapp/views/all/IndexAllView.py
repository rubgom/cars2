import random
from django.forms import formset_factory

from django.shortcuts import render, render_to_response
from django.views.generic import View
from sharingcarapp.forms.StopForm import StopForm


class IndexAllView(View):

    @staticmethod
    def index(request):
        return render(request,'index/index.html')



