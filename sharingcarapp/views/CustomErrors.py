__author__ = 'juanlu'
from django.views.generic import View
from django.shortcuts import render
from django.utils.translation import ugettext as _


class CustomErrors(View):

    @staticmethod
    def error404(request):
        error = _("(404) Page not found")
        return render(request,'errors/variable_error.html', {'error' : error})

    @staticmethod
    def error403(request):
        error = _("(403) Forbidden")
        return render(request,'errors/variable_error.html', {'error' : error})

    @staticmethod
    def error500(request):
        error = _("(500) Server error")
        return render(request, 'errors/variable_error.html', {'error': error,
                                                              'server_error': True})