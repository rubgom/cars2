from django.conf.urls import patterns, include, url
from django.contrib import admin
from sharingcarapp.views.Users.ApplyRouteUserView import ApplyRouteUserView
from sharingcarapp.views.Users.RouteUserView import RouteUserView
from sharingcarapp.views.Users.StopRouteUserView import StopRouteUserView
from sharingcarapp.views.Users.UserUserView import UserUserView
from sharingcarapp.views.Users.AnnouncementUserView import AnnouncementUserView
from sharingcarapp.views.Users.StopAnnouncementUserView import StopAnnouncementUserView
from sharingcarapp.views.all.IndexAllView import IndexAllView
from sharingcarapp.views.all.RouteAllView import RouteAllView

urlpatterns = patterns('',
    url(r'^$', IndexAllView.index, name='index'),
    url(r'^login/?$', 'django.contrib.auth.views.login',{'template_name': 'login.html'}),
    url(r'^logout/?$', 'django.contrib.auth.views.logout', {'next_page': '/'}),
    url(r'^route/user/create$', RouteUserView.create, name='create_route'),
    url(r'^route/user/addStop', StopRouteUserView.create, name='create_stop_route'),
    url(r'^route/user/saveStop$', StopRouteUserView.saveStop, name='save_stop_route'),
    url(r'^route/user/saveMoreStop$', StopRouteUserView.saveMoreStop, name='save_more_stop_route'),
    url(r'^route/user/save$', RouteUserView.save, name='save_route'),
    url(r'^user/register$', UserUserView.create, name='user_register'),
    url(r'^user/save$', UserUserView.save, name='user_save'),
    url(r'^route/all$', RouteAllView.all, name='all_routes'),
    url(r'^route/show', RouteAllView.show, name='show_routes'),
    url(r'^route/user/apply', ApplyRouteUserView.create, name='create_apply_route'),
    url(r'^route/user/saveApply$', ApplyRouteUserView.save, name='save_apply_route'),
    url(r'^route/user/recommendations', UserUserView.recommendations, name='recommendations'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^announcement/user/create$', AnnouncementUserView.create, name='create_announcement'),
    url(r'^announcement/user/save$', AnnouncementUserView.save, name='save_announcement'),
    url(r'^announcement/user/addStop$', StopAnnouncementUserView.create, name='save_more_stop_announcement'),
    url(r'^announcement/user/saveStop$', StopAnnouncementUserView.saveStop, name='save_stop_announcement'),
    url(r'^announcement/user/saveMoreStop$', StopAnnouncementUserView.saveMoreStop, name='save_more_stop_announcement'),
)
